
/**
 * MeuExemploHTTPRequisicoes
 *
 * Metodo GET: faz o 'append' de parametros na URL: ...?firstname=<valor1>&lastname=<valor2>
 *
 * Baseado em: http://otndnld.oracle.co.jp/document/products/as10g/101300/B25221_03/web.1013/b14426/develop.htm
 *
 * Autor: Lucio Agostinho Rocha
 * Ultima atualizacao: 20/04/2017
 *
 **/

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.io.FileInputStream;

public class MeuExemplo1 extends HttpServlet {

    private static final long serialVersionUID = 1L;

 public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String titulo = "ServletDiversos";

        out.println("<!DOCTYPE html><html>");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\" />");

        out.println("<title>" + titulo + "</title>");
        out.println("</head>");
        out.println("<body bgcolor=\"white\">");

        //Inicia a interacao com a aplicacao Servidor
        String mensagem="Servlet: ServletDiversos";
        out.println("<h1>" + mensagem + "</h1><br/>");

        out.println("<h1>" + "Recuperacao de Informacoes da Requisicao" + "</h1><br/>");

        //HTTP POST
        out.println("<h1>" + "HTTP POST" + "</h1><br/>");

        out.print("<form action=\"");
        out.print("MeuExemplo1\" ");
        out.println("method=POST>");
        out.println("Entre o primeiro nome: ");
        out.println("<input type=text size=20 name=firstname>");
        out.println("<br>");
        out.println("Entre o sobrenome: ");
        out.println("<input type=text size=20 name=lastname>");
        out.println("<br>" + "<br>");
        out.println("<input type=submit>");
        out.println("</form>");
        
        
        out.print("<table border=1><tr><td>");
        out.println("Metodo:");
        out.println("</td><td>");
        out.println(request.getMethod());
        out.println("</td></tr><tr><td>");
        out.println("URI da requisicao:");
        out.println("</td><td>");
        out.println(request.getRequestURI());
        out.println("</td></tr><tr><td>");
        out.println("Protocolo:");
        out.println("</td><td>");
        out.println(request.getProtocol());
        out.println("</td></tr>");
        out.println("</table>");

        out.println("</body>");
        out.println("</html>");
    }

}//fim classe
