
/**
 * MeuExemploHTTP_forward
 *
 * - O processamento do servlet atual eh repassado para outro.
 * - Nenhum processamento adicional do servlet original serah
 *   executado apos o encaminhamento.
 *
 * Baseado em: http://otndnld.oracle.co.jp/document/products/as10g/101300/B25221_03/web.1013/b14426/develop.htm
 *
 * Autor: Lucio Agostinho Rocha
 * Ultima atualizacao: 20/04/2017
 *
 **/

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.io.FileInputStream;

public class MeuExemploHTTP_forward extends HttpServlet {

    private static final long serialVersionUID = 1L;

 public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String titulo = "ServletDiversos";

        out.println("<!DOCTYPE html><html>");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\" />");

        out.println("<title>" + titulo + "</title>");
        out.println("</head>");
        out.println("<body bgcolor=\"white\">");

        //Inicia a interacao com a aplicacao Servidor
        String mensagem="Servlet: ServletDiversos";
        out.println("<h1>" + mensagem + "</h1><br/>");

        out.println("<h1>" + "Recuperacao de Informacoes da Requisicao" + "</h1><br/>");

        request.setParameter("firstname","PrimeiroNome");
        request.setParameter("lastname","SobreNome");

        //Encaminha parametros para o outro Servlet:
        getServletConfig().getServletContext().getRequestDispatcher("/servlets/servlet/MeuExemploHTTPGET").forward(request,response);

        out.println("</body>");
        out.println("</html>");
    }

}//fim classe
