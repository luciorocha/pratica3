
/**
 * MeuExemploHTTPGET
 *
 * Metodo GET: faz o 'append' de parametros na URL: ...?firstname=<valor1>&lastname=<valor2>
 *
 * Baseado em: http://otndnld.oracle.co.jp/document/products/as10g/101300/B25221_03/web.1013/b14426/develop.htm
 *
 * Autor: Lucio Agostinho Rocha
 * Ultima atualizacao: 20/04/2017
 *
 **/

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.io.FileInputStream;

public class MeuExemplo1_get extends HttpServlet {

    private static final long serialVersionUID = 1L;

 public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String titulo = "ServletDiversos";

        out.println("<!DOCTYPE html><html>");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\" />");

        out.println("<title>" + titulo + "</title>");
        out.println("</head>");
        out.println("<body bgcolor=\"white\">");

        //Inicia a interacao com a aplicacao Servidor
        String mensagem="Servlet: ServletDiversos";
        out.println("<h1>" + mensagem + "</h1><br/>");

        out.println("<h1>" + "HTTP GET" + "</h1><br/>");

        out.print("<form action=\"");
        out.print("MeuExemploHTTPGET\" ");
        out.println("method=GET>");
        out.println("Entre o primeiro nome: ");
        out.println("<input type=text size=20 name=firstname>");
        out.println("<br>");
        out.println("Entre o sobrenome: ");
        out.println("<input type=text size=20 name=lastname>");
        out.println("<br>" + "<br>");
        out.println("<input type=submit>");
        out.println("</form>");

        //Exibe o conteudo submetido na ultima requisicao com GET nos forms
        String primeiroNome = request.getParameter("firstname");
        String ultimoNome = request.getParameter("lastname");
        if (primeiroNome != null || ultimoNome != null){
           out.println("<br>Primeiro nome: " + primeiroNome);
           out.println("<br>Sobrenome: " + ultimoNome);

        } else {
           out.println("<br>Informe o nome e sobrenome.");
        }

        out.println("</body>");
        out.println("</html>");
    }

}//fim classe
